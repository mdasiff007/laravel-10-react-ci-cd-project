<?php

use App\Http\Controllers\Admin\{
    HomeController
};
use Illuminate\Foundation\Application;
use Illuminate\Support\Facades\Route;

Route::get('/', [HomeController::class, 'index'])->name('admin.home');
